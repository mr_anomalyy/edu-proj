import React from 'react';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from "@react-navigation/stack";
import {
  AddEmployeeScreen,
  EmployeeScreen,
  EmployeesScreen,
  LoginScreen,
  StartupScreen
} from './src/screens';
import createPersistStore from "./src/store";


const persistStore = createPersistStore();
const Stack = createStackNavigator();
const App = () => {
  return (
    <Provider store={persistStore.store}>
      <PersistGate loading={null} persistor={persistStore.persistor}>
        <NavigationContainer>
          <Stack.Navigator headerMode={"none"}>
            <Stack.Screen
              name={"Startup"}
              component={StartupScreen}
            />
            <Stack.Screen
              name={"Login"}
              component={LoginScreen}
            />
            <Stack.Screen
              name={"Employees"}
              component={EmployeesScreen}
            />
            <Stack.Screen
              name={"Employee"}
              component={EmployeeScreen}
            />
            <Stack.Screen
              name={"AddEmployee"}
              component={AddEmployeeScreen}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
