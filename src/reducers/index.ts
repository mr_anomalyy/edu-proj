import { ImageSourcePropType } from "react-native";
import { AnyAction } from "redux";

export interface IEmployee {
  name: string;
  description?: string;
  avatar?: ImageSourcePropType;
}

export interface IEmployeeWithId extends IEmployee {
  id: string;
}

export interface AppStore {
  employees: Record<string, IEmployee | undefined>;
}


export interface UpdateEmployeeAction {
  type: "UPDATE_EMPLOYEE_ACTION";
  payload?: Partial<IEmployeeWithId>;
}

export interface RemoveEmployeeAction {
  type: "REMOVE_EMPLOYEE_ACTION";
  /** employee id */
  payload?: string;
}

export type ActionType = UpdateEmployeeAction
  | RemoveEmployeeAction | AnyAction

const initialState: AppStore = {
  employees: {
    "1": { 
      avatar: require("../../assets/baranov.png"),
      name: "Баранов Алексей Викторович",
      description: "Телефон: +35955977731" +
      "\nРодной город: Мурманск" +
      "\nДолжность: Оператор" +
      "\nДата рождения: 26.01.1980" +
      "\nСтаж работы: 12 лет" +
      "\nОбразование: Среднее-специальное техническое" +
      "\nНаходится на повышаемой должности",
    },
    "2": {
      avatar: require("../../assets/andreeva.png"),
      name: "Андреева Снежана Сергеевна",
      description: "Телефон: +35955977731" +
      "\nРодной город: Чебоксары" +
      "\nДолжность: Старший оператор" +
      "\nДата рождения: 5.01.1980" +
      "\nСтаж работы: 8 лет" +
      "\nОбразование: Среднее-специальное техническое",
    },
    "3": { 
      avatar: require("../../assets/ranevskaya.png"),
      name: "Раневская Алевтина Петровна",
      description: "Телефон: +35955977731" +
      "\nРодной город: Набережные Челны" +
      "\nДолжность: Бухгалтер" +
      "\nДата рождения: 2.05.1990" +
      "\nСтаж работы: 4 года" +
      "\nОбразование: Среднее-специальное техническое" +
      "\nНаходится на повышаемой должности",
    },
    "4": { 
      name: "Абросимов Юрий Степанович",
      description: "Телефон: +35955977731" +
      "\nРодной город: Чебоксары" +
      "\nДолжность: Техник-программист" +
      "\nДата рождения: 26.01.1980" +
      "\nСтаж работы: 13 лет" +
      "\nОбразование: Среднее-специальное техническое" +
      "\nНаходится на повышаемой должности",
    },
    "5": { 
      name: "Степашкин Михаил Андреевич",
      description: "Телефон: +35955979740" +
      "\nРодной город: Мурманск" +
      "\nДолжность: Экономист" +
      "\nДата рождения: 21.01.1981" +
      "\nСтаж работы: 9 лет" +
      "\nОбразование: Среднее-специальное техническое" +
      "Находится на повышаемой должности",
    },
    "6": { 
      name: "Вульф Регина Викторовна",
    },
    "7": { 
      name: "Абрамова Айсылу Завдатовна",
    },
    "8": { 
      name: "Михайлов Михаил Артемьевич",
    },
    "9": { 
      name: "Копытин Адольф Вильевич",
    },
    "10": { 
      name: "Лаврентьев Федор Денисович",
    },
    "11": { 
      name: "Денисович Архип Камилевич",
    },
  },
}

// Use the initialState as a default value
export default function appReducer(state: AppStore = initialState, action: ActionType) {
  switch (action.type) {
    case "REMOVE_EMPLOYEE_ACTION": {
      const employees = { ...state.employees };
      if (action.payload) {
        employees[action.payload] = undefined;
      }
      return {
        ...state,
        employees
      };
    }
    case "UPDATE_EMPLOYEE_ACTION": {
      const employees = { ...state.employees };
      const a = action as UpdateEmployeeAction;
      if (a.payload?.id) {
        const userData = {
          ...employees[a.payload.id],
          ...a.payload,
          id: undefined
        } as IEmployee;
        employees[a.payload.id] = userData;
      }
      return {
        ...state,
        employees
      }
    }
    default:
      return state
  }
}