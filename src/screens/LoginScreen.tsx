import React, { useEffect, useState } from "react";
import {
  Alert, Button, Image,
  Keyboard, StyleSheet, Text,
  View, ViewStyle, TextInput
} from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { LOGIN_CORRECT_VALUE, PASSWORD_CORRECT_VALUE } from "../Constants";

export interface LoginScreenProps {
  navigation: StackNavigationProp<any>;
}

export const LoginScreen = ({ navigation }: LoginScreenProps) => {
  const [keyboardOpened, setKeyboardOpened] = useState(false);
  const [loginValue, setLoginValue] = useState("admin");
  const [passwordValue, setPasswordValue] = useState("admin");
  useEffect(() => {
    const didShow = Keyboard.addListener("keyboardDidShow", () => setKeyboardOpened(true));
    const didHide = Keyboard.addListener("keyboardDidHide", () => setKeyboardOpened(false));

    return () => {
      didShow.remove();
      didHide.remove();
    }
  })
  const bottomContainerStyles: ViewStyle[] = [LoginScreenStyles.bottomContainer];
  if (keyboardOpened) {
    bottomContainerStyles.push({ flex: 1 });
  }
  const handleOnLogin = () => {
    if (loginValue === LOGIN_CORRECT_VALUE && passwordValue === PASSWORD_CORRECT_VALUE) {
      navigation.replace("Employees");
      return;
    }
    Alert.alert("Ошибка", "Неверно введен пароль");
  };
  return (
    <View style={LoginScreenStyles.wrapper}>
      <View style={LoginScreenStyles.upperContainer}>
        <Image
          source={require("../../assets/loginScreenBg.png")}
          style={LoginScreenStyles.upperContainerBg}
          resizeMode={"cover"}
        />
        <View style={LoginScreenStyles.textFirstPartWrapper}>
          <Text style={LoginScreenStyles.text}>{"Добро"}</Text>
        </View>
        <View style={LoginScreenStyles.textSecondPartWrapper}>
          <Text style={LoginScreenStyles.text}>{"пожаловать"}</Text>
        </View>
      </View>
      <View style={bottomContainerStyles}>
        <TextInput
          placeholder={"Логин"}
          style={LoginScreenStyles.input}
          value={loginValue}
          onChangeText={setLoginValue}
        />
        <TextInput
          placeholder={"Пароль"}
          style={LoginScreenStyles.input}
          value={passwordValue}
          secureTextEntry={true}
          onChangeText={setPasswordValue}
        />
        <View style={LoginScreenStyles.buttonWrapper}>
          <Button
            onPress={handleOnLogin}
            title={"Авторизоваться"}
            color={"#FF4D00"}
          />
        </View>
      </View>
    </View>
  )
}

export const LoginScreenStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  upperContainer: {
    flex: 0.7,
    width: "100%",
  },
  upperContainerBg: {
    position: "absolute",
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
  },
  bottomContainer: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonWrapper: {
    paddingTop: 15,
  },
  input: {
    borderBottomColor: "#555555",
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: "50%",
  },
  text: {
    fontSize: 36,
    color: "#FFFFFF",
  },
  textFirstPartWrapper: {
    position: "absolute",
    left: 25,
    top: "25%",
  },
  textSecondPartWrapper: {
    position: "absolute",
    right: 10,
    bottom: "25%",
  },
});
