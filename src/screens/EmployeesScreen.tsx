import React, { useState } from "react";
import { Pressable, StyleSheet, Text, View, ScrollView, TextInput } from "react-native"
import { StackNavigationProp } from "@react-navigation/stack"
import { useDispatch, useSelector } from "react-redux";
import { ActionType, AppStore, IEmployee } from "../reducers";

export interface EmployeesScreenProps {
  navigation: StackNavigationProp<any>;
}

export const EmployeesScreen = ({ navigation }: EmployeesScreenProps) => {
  const [searchQuery, setSearchQuery] = useState("");
  const employeesMap = useSelector(
    (state: AppStore) => state.employees
  );
  const onPressEmployee = (employeeId: string) => {
    navigation.navigate("Employee", { employeeId });
  }
  const onPressAdd = () => {
    navigation.navigate("AddEmployee");
  }
  const renderEmployee = (id: string, employee?: IEmployee) => {
    if (!employee) {
      return null;
    }
    return (
      <Pressable
        key={id}
        style={EmployeesScreenStyles.employeeRow}
        onPress={onPressEmployee.bind(this, id)}
        android_ripple={{
          color: "#000000",
        }}
      >
        <Text style={EmployeesScreenStyles.employeeRowText}>
          {employee.name}
        </Text>
      </Pressable>
    );
  }
  const renderAddButton = () => (
    <Pressable
      key={"add"}
      style={[EmployeesScreenStyles.employeeRow, EmployeesScreenStyles.addRow]}
      onPress={onPressAdd}
      android_ripple={{
        color: "#000000",
      }}
    >
      <Text style={EmployeesScreenStyles.employeeRowText}>
        {"+ Добавить"}
      </Text>
    </Pressable>
  );
  const renderEmployees = (employeesMap: Record<string, IEmployee | undefined>) => {
    return Object.keys(employeesMap)
      .filter((eKey) => employeesMap[eKey]?.name.includes(searchQuery))
      .map((employeeId) => renderEmployee(employeeId, employeesMap[employeeId]))
  }
  return (
    <View style={EmployeesScreenStyles.wrapper}>
      <View style={EmployeesScreenStyles.upperContainer}>
        <Text style={EmployeesScreenStyles.titleText}>
          {"Еще раз добро пожаловать в наше приложение!"}
        </Text>
        <Text style={EmployeesScreenStyles.subTitleText}>
          {"Здесь будет содержаться информация о сотрудниках для составления справок, отчетов и прочей документации."}
        </Text>
      </View>
      <View style={EmployeesScreenStyles.bottomContainer}>
        <Text style={EmployeesScreenStyles.bottomContainerTitleText}>
          {"Ниже представлены сотрудники нашего предприятия:"}
        </Text>
        <TextInput
          onChangeText={setSearchQuery}
          style={EmployeesScreenStyles.searchBar}
          placeholder={"Поиск по сотрудникам"}
        />
        <ScrollView
          indicatorStyle={"white"}
          style={EmployeesScreenStyles.employeesContainer}
        >
          {renderEmployees(employeesMap)}
          {renderAddButton()}
        </ScrollView>
      </View>
    </View>
  )
}

export const EmployeesScreenStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  upperContainer: {
    flex: 0.3,
    paddingTop: 24,
    paddingHorizontal: 30
  },
  bottomContainer: {
    flex: 0.7
  },
  titleText: {
    color: "#FF4D00",
    fontSize: 26,
    textAlign: "center",
  },
  subTitleText: {
    fontSize: 16,
    paddingVertical: 16,
  },
  bottomContainerTitleText: {
    fontSize: 16,
    paddingBottom: 16,
    paddingHorizontal: 30
  },
  employeesContainer: {
    flex: 1,
    backgroundColor: "#FF4D00",
  },
  employeeRow: {
    flexDirection: "row",
    paddingVertical: 14,
    paddingHorizontal: 30,
    width: "100%",
  },
  addRow: {
    justifyContent: "flex-end",
    backgroundColor: "#fff",
  },
  employeeRowText: {
    fontSize: 18,
  },
  searchBar: {
    fontSize: 20,
    paddingHorizontal: 30,
    marginBottom: 8,
    borderBottomColor: "#555555",
    borderBottomWidth: StyleSheet.hairlineWidth,
  }
});
