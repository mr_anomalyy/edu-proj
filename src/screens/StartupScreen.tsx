import React, { useEffect, useRef } from "react";
import { Image, StyleSheet } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";

export interface StartupScreenProps {
  navigation: StackNavigationProp<any>;
}

export const StartupScreen = ({ navigation }: StartupScreenProps) => {
  const didMountRef = useRef(false);
  useEffect(() => {
    if (!didMountRef.current) {
      didMountRef.current = true;
      new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 300);
      })
      .then(() => { navigation.replace("Login"); });
    }
  });
  return (
    <Image
      source={require('../../assets/startupScreenBg.png')}
      resizeMode={"cover"}
      style={StartupScreenStyles.startupImage}
    />
  );
}

export const StartupScreenStyles = StyleSheet.create({
  startupImage: {
    width: "100%",
    height: "100%",
  }
});
