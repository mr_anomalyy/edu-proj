import React from "react";
import { StackScreenProps } from "@react-navigation/stack";
import { Alert, Button, Image, ScrollView, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { AppNavigationParamsList } from "../Navigation";
import { AppStore, RemoveEmployeeAction } from "../reducers";
import { Header } from "react-native/Libraries/NewAppScreen";

export interface EmployeeScreenParams {
  employeeId: string;
}

export interface EmployeeScreenProps extends StackScreenProps<AppNavigationParamsList, "Employee"> { }

export const EmployeeScreen = (props: EmployeeScreenProps) => {
  const dispatch = useDispatch();
  const employee = useSelector((state: AppStore) => state.employees[props.route.params.employeeId]);
  if (!employee) {
    props.navigation.goBack();
    return null;
  }
  const onDelete = () => {
    const onConfirm = () => {
      dispatch({
        type: "REMOVE_EMPLOYEE_ACTION",
        payload: props.route.params.employeeId
      } as RemoveEmployeeAction);
    }
    Alert.alert(
      "Внимание",
      `Вы уверены, что хотите удалить запись о пользователе "${employee.name}"?`,
      [
        { text: "Отмена" },
        { text: "Удалить навсегда", style: "destructive", onPress: onConfirm }
      ]
    );
  }
  return (
    <View style={EmployeeScreenStyles.wrapper}>
      <ScrollView
        style={EmployeeScreenStyles.userScrollWrapper}
        contentContainerStyle={EmployeeScreenStyles.userContainer}
      >
        <View style={EmployeeScreenStyles.userImageWrapper}>
          <Image
            source={employee.avatar || require("../../assets/userIcon.png")}
            style={EmployeeScreenStyles.userImage}
            resizeMode={"contain"}
          />
        </View>
        <Text style={EmployeeScreenStyles.userNameText}>{employee.name}</Text>
        <Text style={EmployeeScreenStyles.userDescriptionText}>
          {employee.description}
        </Text>
      </ScrollView>
      <View style={EmployeeScreenStyles.controlsContainer}>
        <View style={EmployeeScreenStyles.buttonWrapper}>
          <Button
            title={"Удалить"}
            color={"#FF4D00"}
            onPress={onDelete}
          />
        </View>
      </View>
    </View>
  );
}

export const EmployeeScreenStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  userScrollWrapper: {
    flex: 0.9,
  },
  userContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  userImage: {
    width: 84,
    height: 84,
  },
  userImageWrapper: {
    paddingVertical: 36,
  },
  userNameText: {
    fontSize: 24,
    paddingHorizontal: 30,
    textAlign: "center",
  },
  userDescriptionText: {
    width: "100%",
    fontSize: 18,
    lineHeight: 40,
    marginVertical: 18,
    paddingHorizontal: 40,
  },
  controlsContainer: {
    flex: 0.1,
    justifyContent: "center",
    width: "50%",
    alignSelf: "center"
  },
  buttonWrapper: {
  },
});
