import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button, StyleSheet, View, TextInput, Alert } from "react-native";
import { StackScreenProps } from "@react-navigation/stack";
import { AppNavigationParamsList } from "../Navigation";
import { UpdateEmployeeAction } from "../reducers";

export interface AddEmployeeProps extends StackScreenProps<AppNavigationParamsList, "AddEmployee"> { }

export const AddEmployeeScreen = ({ navigation }: AddEmployeeProps) => {
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [surname, setSurName] = useState("");
  const dispatch = useDispatch();
  const onCreate = () => {
    if (!lastName || !firstName || !surname) {
      Alert.alert(
        "Ошибка",
        "Одно или несколько полей не заполнены"
      );
      return;
    }
    dispatch({
      type: "UPDATE_EMPLOYEE_ACTION",
      payload: {
        id: new Date().getTime().toString(),
        name: `${lastName} ${firstName} ${surname}`,
      }
    } as UpdateEmployeeAction);
    navigation.goBack();
  }
  return (
    <View style={AddEmployeeStyles.wrapper}>
      <TextInput
        placeholder={"Фамилия"}
        style={AddEmployeeStyles.input}
        value={lastName}
        onChangeText={setLastName}
      />
      <TextInput
        placeholder={"Имя"}
        style={AddEmployeeStyles.input}
        value={firstName}
        onChangeText={setFirstName}
      />
      <TextInput
        placeholder={"Отчество"}
        style={AddEmployeeStyles.input}
        value={surname}
        onChangeText={setSurName}
      />
      <View style={AddEmployeeStyles.buttonWrapper}>
        <Button
          onPress={onCreate}
          title={"Создать"}
          color={"#FF4D00"}
        />
      </View>
    </View>
  );
}

export const AddEmployeeStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    borderBottomColor: "#555555",
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: "70%",
    marginVertical: 28
  },
  buttonWrapper: {
    marginVertical: 24,
  }
});
