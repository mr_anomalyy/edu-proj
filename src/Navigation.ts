import { EmployeeScreenParams } from "./screens";

export type AppNavigationParamsList = {
  Startup: undefined;
  Login: undefined;
  Employees: undefined;
  Employee: EmployeeScreenParams;
  AddEmployee: undefined;
}